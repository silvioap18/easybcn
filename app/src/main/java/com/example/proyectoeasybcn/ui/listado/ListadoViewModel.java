package com.example.proyectoeasybcn.ui.listado;

import android.app.Application;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.proyectoeasybcn.apiData.AppDatabase;
import com.example.proyectoeasybcn.apiData.LocationDBAPI;
import com.example.proyectoeasybcn.dao.LocationDao;
import com.example.proyectoeasybcn.databinding.FragmentListadoBinding;
import com.example.proyectoeasybcn.pojo.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ListadoViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final LocationDao locationDao;

    public ListadoViewModel(@NonNull Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.locationDao = appDatabase.getLocationDao();
    }


    public LiveData<List<Location>> getLocation(String name) {
        return locationDao.getLocations(name);
    }

    public void reload() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            ArrayList<Location> locations = api.escogerTipoDeUbicacion("1");
            locationDao.deleteLocations();
            locationDao.addLocation(locations);

        });
    }

    public boolean reload(String option, FragmentListadoBinding binding) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        binding.progressBar.setVisibility(ProgressBar.VISIBLE);
        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            locationDao.deleteLocations();
            ArrayList<Location> locations = api.escogerTipoDeUbicacion(option);
            for (int i = 0; i < locations.size(); i++) {
                int id = 1000 + i;
                locations.get(i).set_id(id);
                Log.d("LOCATION_ADD:", locations.get(i).toString());
                locationDao.addLocation(locations.get(i));
            }
            binding.progressBar.setVisibility(ProgressBar.INVISIBLE);
        });
        return true;
    }

    // TODO: Implement the ViewModel

}