package com.example.proyectoeasybcn.ui.generar_punto;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GenerarPuntoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public GenerarPuntoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}