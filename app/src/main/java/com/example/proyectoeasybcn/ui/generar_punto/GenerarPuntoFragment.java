package com.example.proyectoeasybcn.ui.generar_punto;

import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.example.proyectoeasybcn.MainActivity;
import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.databinding.FragmentGenerarPuntoBinding;
import com.example.proyectoeasybcn.pojo.Location;
import com.example.proyectoeasybcn.sharedViewModel.SharedViewModel;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.data.model.Resource;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class GenerarPuntoFragment extends Fragment {

    private SharedViewModel model;

    String mCurrentPhotoPath;
    private Uri photoURI;
    static final int REQUEST_TAKE_PHOTO = 1;

    FirebaseStorage storage;
    StorageReference storageRef;
    String downloadUrl;

    Button button;

    boolean[] selectType;

    FragmentGenerarPuntoBinding binding;

    public GenerarPuntoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.activity_main_drawer, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.signOutButton){
            AuthUI.getInstance()
                    .signOut(requireContext())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            FirebaseAuth auth = FirebaseAuth.getInstance();
                            if (auth.getCurrentUser() == null) {
                                // Choose authentication providers
                                startActivityForResult(
                                        AuthUI.getInstance()
                                                .createSignInIntentBuilder()
                                                /*.setLogo()*/
                                                .setAvailableProviders(
                                                        Arrays.asList(
                                                                new AuthUI.IdpConfig.EmailBuilder()
                                                                        /*.enableEmailLinkSignIn()
                                                                        .setAllowNewAccounts(true)*/
                                                                        .build(),
                                                                new AuthUI.IdpConfig.GoogleBuilder()
                                                                        /*.setSignInOptions(gso)*/
                                                                        .build()
                                                        )
                                                )
                                                .build(),
                                        0);
                            }
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentGenerarPuntoBinding.inflate(inflater);

        View view = binding.getRoot();


        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);


        model.getCurrentAddress().observe(getViewLifecycleOwner(), address -> {
            binding.txtDireccio.setText(getString(R.string.address_text,
                    address));
        });
        model.getCurrentLatLng().observe(getViewLifecycleOwner(), latlng -> {
            binding.txtLatitud.setText(String.valueOf(latlng.latitude));
            binding.txtLongitud.setText(String.valueOf(latlng.longitude));
        });
        model.getProgressBar().observe(getViewLifecycleOwner(), visible -> {
            if (visible)
                binding.loading.setVisibility(ProgressBar.VISIBLE);
            else
                binding.loading.setVisibility(ProgressBar.INVISIBLE);
        });
        model.switchTrackingLocation();

        Resources res = getResources();
        String[] tiposUbicaciones = res.getStringArray(R.array.reply_entries);
        tiposUbicaciones[0] = "";

        button = view.findViewById(R.id.button_type);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setTitle("Tipo: ");
                builder.setCancelable(false);
                builder.setSingleChoiceItems(tiposUbicaciones, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        binding.txtType.setText(tiposUbicaciones[i]);
                    }
                });
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        });

        binding.buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();

        });

        binding.buttonNotificar.setOnClickListener(button -> {

            storage = FirebaseStorage.getInstance("gs://easybcn-180222.appspot.com");
            storageRef = storage.getReference();

            Log.d(TAG, "onCreateView: HASTA AQUI LLEGA");

            if (mCurrentPhotoPath != null){
                StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
                UploadTask uploadTask = imageRef.putFile(photoURI);

                uploadTask.addOnSuccessListener(taskSnapshot -> {
                    Log.d("METADATA", Objects.requireNonNull(taskSnapshot.getMetadata()).toString());
                    imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                        Uri downloadUri = task.getResult();
                        Glide.with(this).load(downloadUri).into(binding.foto);
                        downloadUrl = downloadUri.toString();

                        Location location = new Location();
                        location.setNombre(binding.txtNombre.getText().toString());
                        location.setNombre_calle(binding.txtDireccio.getText().toString());
                        location.setCoordenada_x(binding.txtLatitud.getText().toString());
                        location.setCoordenada_y(binding.txtLongitud.getText().toString());
                        location.setAddresses_town(downloadUrl);

                        String[] tiposUbicacionesValue = res.getStringArray(R.array.reply_values);
                        for (int i = 0; i < tiposUbicaciones.length; i++) {
                            String txt = binding.txtType.getText().toString();
                            if (tiposUbicaciones[i].equals(txt)){
                                location.setTypeLocation(tiposUbicacionesValue[i]);
                                break;
                            }
                        }



                        /*Latitud: coordenada X
                        * Longitud: coordenada Y */

                        DatabaseReference base = FirebaseDatabase
                                .getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                                .getReference();

                        DatabaseReference locations = base.child("locations");
                        DatabaseReference reference = locations.push();
                        reference.setValue(location);

                        Toast.makeText(getContext(), "Localizacion publicada", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, " LOCALIZACION PUBLICADA: ------------------------------------------");


                    });

                });
            }else {
                Toast.makeText(getContext(),
                        "Es necessari una foto!", Toast.LENGTH_SHORT).show();
            }



        });

        return view;
    }



    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                requireContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.e("ERROR FOTO", e.getMessage());
                e.printStackTrace();
            }


            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(requireContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(photoURI).into(binding.foto);
            } else {
                Toast.makeText(getContext(),
                        "No se ha hecho la foto!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}