package com.example.proyectoeasybcn.ui.my_actividades;

import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.databinding.FragmentListActividadesBinding;
import com.example.proyectoeasybcn.databinding.MyActividadesFragmentBinding;
import com.example.proyectoeasybcn.pojo.Actividades;
import com.example.proyectoeasybcn.ui.list_actividades.ListActividadesViewModel;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class MyActividadesFragment extends Fragment {

    private MyActividadesViewModel mViewModel;
    private MyActividadesFragmentBinding binding;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putAll(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    /*public void listData(){


        FirebaseListOptions<Actividades> options = new FirebaseListOptions.Builder<Actividades>()
                .setQuery(actividades, Actividades.class)
                .setLayout(R.layout.lv_actividades_row)
                .setLifecycleOwner(this)
                .build();


        binding.lvActividades.setAdapter(adapter);
    }*/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = MyActividadesFragmentBinding.inflate(inflater);

        View view = binding.getRoot();


        FirebaseAuth auth = FirebaseAuth.getInstance();
        String user = auth.getUid();
        DatabaseReference base = FirebaseDatabase.
                getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                .getReference();
        DatabaseReference usuarios = base.child("users").child(user);


        FirebaseListOptions<Actividades> adapterFirebase = new FirebaseListOptions.Builder<Actividades>()
                .setQuery(usuarios, Actividades.class)
                .setLayout(R.layout.lv_actividades_row)
                .setLifecycleOwner(this)
                .build();

        FirebaseListAdapter<Actividades> adapter = new FirebaseListAdapter<Actividades>(adapterFirebase) {
            @SuppressLint("SetTextI18n")
            @Override
            protected void populateView(@NonNull View v, @NonNull Actividades model, int position) {
                TextView actividadNombre = v.findViewById(R.id.actividadNombre);
                TextView actividadDireccion = v.findViewById(R.id.actividadDireccion);
                TextView actividadFecha = v.findViewById(R.id.actividadFecha);
                TextView actividadNumParticipantes = v.findViewById(R.id.actividadNumParticipantes);
                actividadNombre.setText(model.getNombre());
                actividadDireccion.setText("Direccion: " + model.getDireccion());
                actividadFecha.setText("Fecha: " + model.getDate());
                String numParticipantes = String.valueOf(model.getNumParticipantes());
                actividadNumParticipantes.setText("Nº participantes: " + numParticipantes);
            }

        };

        binding.lvActividades.setAdapter(adapter);
        return view;
    }

}