package com.example.proyectoeasybcn.ui.listado;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.apiData.LocationAdapter;
import com.example.proyectoeasybcn.databinding.FragmentListadoBinding;
import com.example.proyectoeasybcn.pojo.Location;
import com.example.proyectoeasybcn.sharedViewModel.SharedViewModel;
import com.example.proyectoeasybcn.ui.settings.SettingsActivity;
import com.example.proyectoeasybcn.utilidades.DrawableId;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ListadoFragment extends Fragment {

    private ListadoViewModel listadoViewModel;
    private FragmentListadoBinding binding;
    private SharedPreferences preferences;

    private ArrayList<Location> items;
    private List<Location> locations = new ArrayList<>();
    private LocationAdapter adapter;
    private String option;
    private static Boolean nightMode;


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putAll(outState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentListadoBinding.inflate(inflater);

        View view = binding.getRoot(); /*inflater.inflate(R.layout.main_fragment, container, false);*/

        SharedPreferences preferences = androidx.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("name", "");
        option = preferences.getString("escoger_contenido","");

        items = new ArrayList<>();

        adapter = new LocationAdapter(
                getContext(),
                R.layout.lv_easybcn_row,
                items
        );

        listadoViewModel = new ViewModelProvider(this).get(ListadoViewModel.class);

        if (locations.size() == 0){
            if (option.equals("9")){
                DatabaseReference base = FirebaseDatabase
                        .getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                        .getReference();
                DatabaseReference locations = base.child("locations");

                FirebaseListOptions<Location> options = new FirebaseListOptions.Builder<Location>()
                        .setQuery(locations, Location.class)
                        .setLayout(R.layout.lv_easybcn_row)
                        .setLifecycleOwner(this)
                        .build();

                FirebaseListAdapter<Location> adapterFirebase = new FirebaseListAdapter<Location>(options){
                    @SuppressLint("SetTextI18n")
                    @Override
                    protected void populateView(@NonNull View v, @NonNull Location model, int position) {
                        TextView locationTitle = v.findViewById(R.id.locationTitle);
                        TextView locationInfo = v.findViewById(R.id.locationInfo);
                        ImageView locationImage = v.findViewById(R.id.locationImage);

                        locationTitle.setText(model.getNombre());
                        locationInfo.setText(model.getNombre_calle() + " " + model.getAddresses_road_id());
                        locationImage.setImageResource(LocationAdapter.putIcon(model));
                    }
                };
                binding.lvLocations.setAdapter(adapterFirebase);
            }else {
                listadoViewModel.getLocation(name).observe(getViewLifecycleOwner(), locations -> {
                    adapter.clear();
                    adapter.addAll(locations);
                });
                listadoViewModel.reload(option, binding);
                binding.lvLocations.setAdapter(adapter);
            }
        }


        SharedViewModel sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);



        binding.lvLocations.setOnItemClickListener((parent, view1, position, id) ->{
            Location location = adapter.getItem(position);
            if (!esTablet()){
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("location", location);
                startActivity(intent);
            }else {
                sharedViewModel.select(location);
            }

        });


        binding.lvTitle.setText(putTitleName());
        binding.lvIcon.setImageResource(putIcon());

        /*Log.e("LOCATIONS SIZE: ", "onCreateView: " + locations.size() );*/

        return view;
    }

    public String putTitleName(){
        if (option == null || option.equals("") || option.equals("true")){
            return "";
        }
        int numTitle = Integer.parseInt(option);
        /*numTitle -= 1;*/
        Resources res = getResources();
        String[] names = res.getStringArray(R.array.reply_entries);
        return names[numTitle];
    }

    public int putIcon(){
        if (option == null || option.equals("") || option.equals("true")){
            return R.drawable.icloc_default;
        }
        int numTitle = Integer.parseInt(option);
        /*numTitle -= 1;*/
        int[] names = DrawableId.ICON_ARRAY;
        return names[numTitle];
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        /*@SuppressLint("UseSwitchCompatOrMaterialCode") Switch sw = (Switch) menu.findItem(R.id.switchNightMode).getActionView()
                .findViewById(R.id.switchNightModeItem);

        if (nightMode){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            sw.setChecked(true);
        }
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    sw.setChecked(true);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("dark_mode", true);
                    editor.commit();
                }else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    sw.setChecked(false);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("dark_mode", false);
                    editor.commit();
                }
            }
        });*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh){
            listadoViewModel.reload(preferences.getString("escoger_contenido",""), binding);
            option = preferences.getString("escoger_contenido","");
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
            listadoViewModel.reload(option, binding);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        preferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        String name = preferences.getString("name", "");
        option = preferences.getString("escoger_contenido", "");
        Log.w("OPTION", option);
        binding.lvTitle.setText(putTitleName());
        binding.lvIcon.setImageResource(putIcon());



        nightMode = preferences.getBoolean("dark_mode", false);
        if (nightMode){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        listadoViewModel = new ViewModelProvider(this).get(ListadoViewModel.class);
        listadoViewModel.getLocation(name).removeObservers(getViewLifecycleOwner());


        if (locations.size() == 0){
            listadoViewModel.getLocation(name).observe(getViewLifecycleOwner(), locations -> {
                adapter.clear();
                adapter.addAll(locations);
            });
        }

    }


    boolean esTablet (){
        return getResources().getBoolean(R.bool.tablet);
    }


}