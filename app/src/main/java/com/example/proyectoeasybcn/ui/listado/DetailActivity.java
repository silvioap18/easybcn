package com.example.proyectoeasybcn.ui.listado;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.proyectoeasybcn.R;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetailFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }
}