package com.example.proyectoeasybcn.ui.generar_actividad;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.databinding.FragmentGenerarActividadBinding;
import com.example.proyectoeasybcn.pojo.Actividades;
import com.example.proyectoeasybcn.sharedViewModel.SharedViewModel;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class GenerarActividadFragment extends Fragment {

    private SharedViewModel model;

    private GenerarActividadViewModel mViewModel;
    FragmentGenerarActividadBinding binding;

    DatePickerDialog.OnDateSetListener setListener;
    Button buttonDate;
    Button buttonNumParticipantes;

    int PLACE_PICKER_REQUEST = 1;

    public GenerarActividadFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.activity_main_drawer, menu);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentGenerarActividadBinding.inflate(inflater);

        View view = binding.getRoot();

        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);


        buttonDate = view.findViewById(R.id.button_date_GA);

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateTimeDialog();
            }
        });

        buttonNumParticipantes = view.findViewById(R.id.button_numParticipantes_GA);
        buttonNumParticipantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberPickerDialog();
            }
        });

        binding.buttonNotificarGA.setOnClickListener(button -> {
            UUID uuid = UUID.randomUUID();
            Actividades actividades = new Actividades();

            actividades.setId(uuid.toString());
            actividades.setDireccion(binding.txtDireccioGA.getText().toString());
            actividades.setNombre(binding.txtNombreGA.getText().toString());
            actividades.setDescripcion(binding.txtDescripcionGA.getText().toString());
            actividades.setDate(binding.txtDateGA.getText().toString());
            actividades.setNumParticipantes(Integer.parseInt(binding.txtNumParticipantesGA.getText().toString()));
            List<String> participantes = new ArrayList<>();
            participantes.add("CreadorActividad");
            actividades.setParticipantes(participantes);

            DatabaseReference base = FirebaseDatabase
                    .getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                    .getReference();

            DatabaseReference locations = base.child("actividades");
            DatabaseReference reference = locations.push();
            reference.setValue(actividades);

            Toast.makeText(getContext(), "Localizacion publicada", Toast.LENGTH_SHORT).show();
            Log.d(TAG, " LOCALIZACION PUBLICADA: ------------------------------------------");

        });

        return view;
    }

    private void numberPickerDialog() {
        NumberPicker numberPicker = new NumberPicker(requireContext());
        numberPicker.setMinValue(2);
        numberPicker.setMaxValue(35);
        NumberPicker.OnValueChangeListener valueChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
               binding.txtNumParticipantesGA.setText(String.valueOf(newVal));
            }
        };
        numberPicker.setOnValueChangedListener(valueChangeListener);
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext()).setView(numberPicker);
        builder.setTitle("Nº de participantes").setIcon(R.drawable.ic_baseline_groups_24);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

            }
        });
        builder.show();
    }

    private void showDateTimeDialog() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");

                        binding.txtDateGA.setText(simpleDateFormat.format(calendar.getTime()));

                    }
                };
                new TimePickerDialog(getContext(), timeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show();
            }
        };
        new DatePickerDialog(getContext(), dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.signOutButton){
            AuthUI.getInstance()
                    .signOut(requireContext())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            FirebaseAuth auth = FirebaseAuth.getInstance();
                            if (auth.getCurrentUser() == null) {
                                // Choose authentication providers
                                startActivityForResult(
                                        AuthUI.getInstance()
                                                .createSignInIntentBuilder()
                                                /*.setLogo()*/
                                                .setAvailableProviders(
                                                        Arrays.asList(
                                                                new AuthUI.IdpConfig.EmailBuilder()
                                                                        /*.enableEmailLinkSignIn()
                                                                        .setAllowNewAccounts(true)*/
                                                                        .build(),
                                                                new AuthUI.IdpConfig.GoogleBuilder()
                                                                        /*.setSignInOptions(gso)*/
                                                                        .build()
                                                        )
                                                )
                                                .build(),
                                        0);
                            }
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }

}