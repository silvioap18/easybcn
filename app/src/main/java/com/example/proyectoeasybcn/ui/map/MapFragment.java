package com.example.proyectoeasybcn.ui.map;

import static android.content.ContentValues.TAG;

import static com.example.proyectoeasybcn.ui.listado.DetailFragment.resizeMapIcons;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.apiData.LocationDBAPI;
import com.example.proyectoeasybcn.databinding.FragmentMapBinding;
import com.example.proyectoeasybcn.pojo.Location;
import com.example.proyectoeasybcn.sharedViewModel.SharedViewModel;
import com.example.proyectoeasybcn.apiData.LocationAdapter;
import com.example.proyectoeasybcn.ui.listado.DetailActivity;
import com.example.proyectoeasybcn.ui.listado.DetailFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MapFragment extends Fragment {

    private MapViewModel mapViewModel;
    private FragmentMapBinding binding;
    private SharedPreferences preferences;

    private String option;
    private ArrayList<Location> items;
    private LocationAdapter adapter;
    private List<Location> mapContent = new ArrayList<>();

    private static final int LOCATION_REQUEST_CODE = 1;
    private static SupportMapFragment mapFragment;
    private GoogleMap mMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);

        binding = FragmentMapBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

        SharedPreferences preferences = androidx.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("name", "");
        option = preferences.getString("escoger_contenido", "");

        if (option.equals("")) option = "1";

        items = new ArrayList<>();

        adapter = new LocationAdapter(
                getContext(),
                R.layout.lv_easybcn_row,
                items
        );

        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);

        List<Location> list = new ArrayList<>();

        if (option == null || option.equals("") || option.equals("true")) {
            mapViewModel.getLocation(name).observe(getViewLifecycleOwner(), locations -> {
                adapter.clear();
                adapter.addAll(locations);
                mapContent.addAll(locations);
            });
        } else {
            mapViewModel.getLocation(name, option).observe(getViewLifecycleOwner(), locations -> {
                adapter.clear();
                adapter.addAll(locations);
                mapContent.addAll(locations);
            });

            SharedViewModel sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);

            mapFragment.getMapAsync(map -> {
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);

                    MutableLiveData<LatLng> currentLatLng = sharedViewModel.getCurrentLatLng();
                    LifecycleOwner owner = getViewLifecycleOwner();
                    currentLatLng.observe(owner, latLng -> {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                        map.animateCamera(cameraUpdate);
                        currentLatLng.removeObservers(owner);
                    });

                    DatabaseReference base = FirebaseDatabase
                            .getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                            .getReference();

                    DatabaseReference locations = base.child("locations");

                    locations.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                            Location location = snapshot.getValue(Location.class);
                            LatLng locationXY = new LatLng(
                                    Double.parseDouble(location.getCoordenada_x()),
                                    Double.parseDouble(location.getCoordenada_y())
                            );
                            Bitmap bitmap = DetailFragment.drawableToBitmap(AppCompatResources.getDrawable(requireContext(), LocationAdapter.putIcon(location)));
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .title(location.getNombre())
                                    .snippet(location.getNombre_calle() + " " + location.getAddresses_road_id())
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                    .position(locationXY).icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(bitmap, 150, 150))));
                            marker.setTag(location);
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });


                    mapContent = mapViewModel.mapValues(option);


                    for (int i = 0; i < mapContent.size(); i++) {
                        Location location = mapContent.get(i);
                        LatLng locationXY = new LatLng(
                                Double.parseDouble(location.getCoordenada_x()),
                                Double.parseDouble(location.getCoordenada_y())
                        );
                        Bitmap bitmap = DetailFragment.drawableToBitmap(AppCompatResources.getDrawable(requireContext(), LocationAdapter.putIcon(location)));

                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(location.getNombre())
                                .snippet(location.getNombre_calle() + " " + location.getAddresses_road_id())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                .position(locationXY).icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(bitmap, 150, 150))));
                        marker.setTag(location);
                    }

                } else {
                    Log.d(TAG, "onCreateView: PROBLEM WITH PERMISSIONS");
                }





            /*try {
                boolean success = map.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                requireActivity(), R.raw.map_style));

                if (!success) {
                    Log.e(null, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(null, "Can't find style. Error: ", e);
            }*/

            });

        }


        return root;
    }

    public SupportMapFragment SupportMapFragment() {
        return (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);
    }

    @Override
    public void onStart() {
        super.onStart();

        preferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        String name = preferences.getString("name", "");
        option = preferences.getString("escoger_contenido", "");
        Log.w("OPTION", option);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}