package com.example.proyectoeasybcn.ui.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.proyectoeasybcn.MainActivity;
import com.example.proyectoeasybcn.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public boolean onPreferenceTreeClick(@NonNull Preference preference) {

        return super.onPreferenceTreeClick(preference);
    }



    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.root_preferences/*, rootKey*/);

        Preference myPref = (Preference) findPreference("easybcn_aboutUs");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setMessage("EasyBcn v.1\n" +
                        "Silvio Félix Alva Pérez ");
                builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return true;
            }
        });
    }


}