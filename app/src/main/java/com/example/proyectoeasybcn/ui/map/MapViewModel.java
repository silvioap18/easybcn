package com.example.proyectoeasybcn.ui.map;

import android.app.Application;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.proyectoeasybcn.apiData.AppDatabase;
import com.example.proyectoeasybcn.apiData.LocationDBAPI;
import com.example.proyectoeasybcn.dao.LocationDao;
import com.example.proyectoeasybcn.databinding.FragmentListadoBinding;
import com.example.proyectoeasybcn.pojo.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MapViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final LocationDao locationDao;

    public MapViewModel(@NonNull Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.locationDao = appDatabase.getLocationDao();
    }

    public LiveData<List<Location>> getLocation(String name) {
        return locationDao.getLocations(name);
    }

    public LiveData<List<Location>> getLocation(String name, String type) {
        return locationDao.getLocations(name, type);
    }

    public void reload() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            ArrayList<Location> locations = api.escogerTipoDeUbicacion("1");
            locationDao.deleteLocations();
            locationDao.addLocation(locations);

        });
    }

    public boolean reload(String option) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            locationDao.deleteLocations();
            ArrayList<Location> locations = api.escogerTipoDeUbicacion(option);
            for (int i = 0; i < locations.size(); i++) {
                locations.get(i).set_id(i);
                Log.d("LOCATION_ADD:",  locations.get(i).toString());
                locationDao.addLocation(locations.get(i));
            }
        });
        return true;
    }

    public boolean fillDao() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            locationDao.deleteLocations();
            ArrayList<Location> locations = api.escogerTipoDeUbicacion("1");
            for (int i = 0; i < locations.size(); i++) {
                locations.get(i).set_id(i);
                Log.d("LOCATION_ADD:",  locations.get(i).toString());
                locationDao.addLocation(locations.get(i));
            }
        });
        return true;
    }

    public List<Location> mapValues(String option) {
        List<Location> locations = new ArrayList<>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            LocationDBAPI api = new LocationDBAPI();
            locationDao.deleteLocations();
            ArrayList<Location> locationsApi = api.escogerTipoDeUbicacion(option);
            for (int i = 0; i < locationsApi.size(); i++) {
                locationsApi.get(i).set_id(i);
                Log.d("MAP value", "mapValues: " + locationsApi.get(i).toString());
                locations.add(locationsApi.get(i));
            }
            executor.shutdown();
        });
        while (!executor.isTerminated()){
            System.out.println(executor.isTerminated());
        }
        return locations;
    }


}