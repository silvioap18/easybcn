package com.example.proyectoeasybcn.ui.listado;

import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.apiData.LocationAdapter;
import com.example.proyectoeasybcn.databinding.DetailFragmentBinding;
import com.example.proyectoeasybcn.pojo.Location;
import com.example.proyectoeasybcn.sharedViewModel.SharedViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;


public class DetailFragment extends Fragment {

    private DetailViewModel mViewModel;
    private DetailFragmentBinding binding;
    private SupportMapFragment mapFragment;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DetailFragmentBinding.inflate(inflater);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.locationDetailMap);

        View view = binding.getRoot();

        Places.initialize(getActivity().getApplicationContext(), String.valueOf(R.string.android_sdk_places_api_key));

        Intent intent = getActivity().getIntent();
        if (intent != null){
            Location location = (Location) intent.getSerializableExtra("location");
            if (location != null){
                showData(location);
            }
        }

        SharedViewModel sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);
        sharedViewModel.getSelected().observe(getViewLifecycleOwner(), this::showData);


        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }


    @SuppressLint("SetTextI18n")
    private void showData(Location location) {


        String name = location.getNombre();
        System.out.println(name);

        binding.locationDetailTitle.setText(name);
        binding.locationDetailStreet.setText("Calle: " + location.getNombre_calle() + " " + location.getAddresses_road_id());
        binding.locationDetailDistrict.setText("Distrito: " + location.getNombre_distrito());
        binding.locationDetailZipCode.setText("Codigo postal: " + location.getCodigo_postal());
        if (!location.getValues_attribute_name().equals("") && !location.getValues_value().equals("null")){
            binding.locationDetailInfo.setText(location.getValues_attribute_name() + " " + location.getValues_value());
        }else binding.locationDetailInfo.setText("");

        mapFragment.getMapAsync(map -> {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(location.getCoordenada_x()),
                    Double.parseDouble(location.getCoordenada_y())), 15);
            map.animateCamera(cameraUpdate);

            LatLng locationXY = new LatLng(
                    Double.parseDouble(location.getCoordenada_x()),
                    Double.parseDouble(location.getCoordenada_y())
            );
            Bitmap bitmap = drawableToBitmap(AppCompatResources.getDrawable(requireContext(), LocationAdapter.putIcon(location)));
            map.clear();
            map.addMarker(new MarkerOptions()
                    .title(name)
                    .snippet(location.getNombre_calle() + " " + location.getAddresses_road_id())
                    .position(locationXY)
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(bitmap, 150, 150)))
            );
        });

        /*Glide.with(getContext()).load(
                location.getImage()
        ).into(binding.ivFunkoImageAct);*/

    }

    public static Bitmap resizeMapIcons(Bitmap bitmap,int width, int height){
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
        return resizedBitmap;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // TODO: Use the ViewModel
    }

}