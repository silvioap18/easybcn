package com.example.proyectoeasybcn.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Actividades implements Serializable {

    static final long serialVersionUID = -3126998878902358585L;

    @PrimaryKey
    private String id;
    private String direccion;
    private String nombre;
    private String descripcion;
    private String date;
    private int numParticipantes;
    private List<String> participantes = new ArrayList<>();

    @Override
    public String toString() {
        return "Actividades{" +
                "id='" + id + '\'' +
                ", direccion='" + direccion + '\'' +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", date='" + date + '\'' +
                ", numParticipantes=" + numParticipantes +
                ", participantes=" + participantes +
                '}';
    }
}
