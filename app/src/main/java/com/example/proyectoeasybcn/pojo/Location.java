package com.example.proyectoeasybcn.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location implements Serializable {

    static final long serialVersionUID = -3126998878902358585L;

    @PrimaryKey
    private int _id;

    private String register_id;
    private String nombre;
    private int institution_id;
    private String institution_name;
    private int addresses_road_id;
    private String nombre_calle;
    private int addresses_neighborhood_id;
    private String nombre_barrio;
    private int addresses_district_id;
    private String nombre_distrito;
    private int codigo_postal;
    private String addresses_town;
    private String values_category;
    private String values_attribute_name;
    private String values_value;
    private String coordenada_x;
    private String coordenada_y;
    private String typeLocation;

    @Override
    public String toString() {
        return "Location{" +
                "_id=" + _id +
                ", register_id='" + register_id + '\'' +
                ", name='" + nombre + '\'' +
                ", institution_id=" + institution_id +
                ", institution_name='" + institution_name + '\'' +
                ", addresses_road_id=" + addresses_road_id +
                ", addresses_road_name='" + nombre_calle + '\'' +
                ", addresses_neighborhood_id=" + addresses_neighborhood_id +
                ", addresses_neighborhood_name='" + nombre_barrio + '\'' +
                ", addresses_district_id=" + addresses_district_id +
                ", addresses_district_name='" + nombre_distrito + '\'' +
                ", addresses_zip_code=" + codigo_postal +
                ", addresses_town='" + addresses_town + '\'' +
                ", values_category='" + values_category + '\'' +
                ", values_attribute_name='" + values_attribute_name + '\'' +
                ", values_value='" + values_value + '\'' +
                ", geo_epgs_4326_x='" + coordenada_x + '\'' +
                ", geo_epgs_4326_y='" + coordenada_y + '\'' +
                ", typeLocation" + typeLocation + '\'' +
                '}';
    }
}
