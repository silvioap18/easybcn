package com.example.proyectoeasybcn.utilidades;

import com.example.proyectoeasybcn.R;

public class DrawableId {
    public static int[] ICON_ARRAY =
            {R.drawable.icloc_default,R.drawable.icloc_consulado, R.drawable.icloc_mirador,
                    R.drawable.icloc_comisaria, R.drawable.icloc_muelle, R.drawable.icloc_playa,
                    R.drawable.icloc_arbol_navidad_recogida, R.drawable.icloc_parque_perros,
                    R.drawable.icloc_deporte_gente_mayor, R.drawable.icloc_default};

}
