package com.example.proyectoeasybcn.apiData;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.proyectoeasybcn.databinding.LvActividadesRowBinding;
import com.example.proyectoeasybcn.pojo.Actividades;

import java.util.List;

public class ActividadAdapter extends ArrayAdapter<Actividades> {

    public ActividadAdapter(@NonNull Context context, int resource, @NonNull List<Actividades> objects) {
        super(context, resource, objects);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Actividades actividades = getItem(position);

        LvActividadesRowBinding binding = null;

        if (convertView == null){
            binding = LvActividadesRowBinding.inflate(
                    LayoutInflater.from(getContext()),
                    parent,
                    false
                    );
        }else {
            binding = LvActividadesRowBinding.bind(convertView);
        }

        binding.actividadNombre.setText(actividades.getNombre());
        binding.actividadDireccion.setText(actividades.getDireccion());

        return binding.getRoot();
    }
}
