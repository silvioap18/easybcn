package com.example.proyectoeasybcn.apiData;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.proyectoeasybcn.dao.LocationDao;
import com.example.proyectoeasybcn.pojo.Location;


@Database(entities = {Location.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class, "location.db"
            ).build();
        }
        return INSTANCE;
    }

    public abstract LocationDao getLocationDao();

}
