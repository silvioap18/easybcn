package com.example.proyectoeasybcn.apiData;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.proyectoeasybcn.R;
import com.example.proyectoeasybcn.pojo.Location;

import com.example.proyectoeasybcn.databinding.LvEasybcnRowBinding;
import com.example.proyectoeasybcn.utilidades.DrawableId;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class LocationAdapter extends ArrayAdapter<Location> {


    public LocationAdapter(@NonNull Context context, int resource, @NonNull List<Location> objects) {
        super(context, resource, objects);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Location location = getItem(position);
        /*Log.w("Location", location.toString());*/

        LvEasybcnRowBinding binding = null;

        if (convertView == null) {
            binding = LvEasybcnRowBinding.inflate(
                    LayoutInflater.from(getContext()),
                    parent,
                    false
            );

        }else {
            binding = LvEasybcnRowBinding.bind(convertView);
        }


        String name = location.getNombre();
        binding.locationTitle.setText(name);
        binding.locationInfo.setText(location.getNombre_calle() + " " + location.getAddresses_road_id());
        binding.locationImage.setImageResource(putIcon(location));

        /*Glide.with(getContext()).load(
                location.getImage()
        ).into(binding.ivFunkoImage);*/

        return binding.getRoot();
    }

    public static int putIcon(Location location){
        if (location.getTypeLocation() == null || location.getTypeLocation().equals("")){
            return R.drawable.icloc_default;
        }
        int numTitle = Integer.parseInt(location.getTypeLocation());
        /*numTitle -= 1;*/
        int[] names = DrawableId.ICON_ARRAY;
        return names[numTitle];
    }

}
