package com.example.proyectoeasybcn.apiData;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.proyectoeasybcn.pojo.Location;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class LocationDBAPI {
    private final String BASE_URL = "https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search";
    private final String API_KEY = "<api-key>";

    /*https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=873e7692-cae0-4c7d-ac44-5cbc99843b39*/

    /*https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=e48b5752-1291-4825-8779-a853832a0f50*/

    /*
    * Id segun contenido:
    *   - Consulados:   873e7692-cae0-4c7d-ac44-5cbc99843b39
    *   - Miradores:    50e946a8-f4a2-4a16-8b40-6553bf4ccd81
    *   - Comisaria:    40ab2784-3555-466a-996c-55f3d900c0e5
    *   - Muelles:      e17799cf-4029-4687-9ed2-e86bebc8fd9e
    *   - Playas:       cc4a6b67-4169-45ae-9f5a-e464908d2417
    *   - Recogida arboles: fd4415e3-b7dc-4173-bb88-685401de191f
    *   - PipiCan:      fd4415e3-b7dc-4173-bb88-685401de191f
    *   - Gimnasia gente mayor: bee7897a-9088-4ab1-ba4b-de7f68cf7fc5
    * */

    //224
    public ArrayList<Location> escogerTipoDeUbicacion(String opcion){
        ArrayList<Location> data = new ArrayList<>();
        switch (opcion){
            case "1":
                data = getConsulats("1");
                break;

            case "2":
                data = getMiradores("2");
                break;

            case "3":
                data = getComisarias("3");
                break;

            case "4":
                data = getMuelles("4");
                break;

            case "5":
                data = getPlayas("5");
                break;

            case "6":
                data = getPuntosDeRecogidaDeArbolesDeNavidad("6");
                break;

            case "7":
                data = getAreasDeRecreoParaPerros("7");
                break;

            case "8":
                data = getEspaciosGimnasiaGenteMayor("8");
                break;

            case "9":
                data = getPuntosComunidad("-1");
                break;

            case "0":
                data = getAll("0");
                break;

            default:
                System.out.println("LOG/ Error! Not existing option.");
                break;
        }
        return data;
    }

    public synchronized ArrayList<Location> getAll(String typeLocation){
        ArrayList<Location> allLocations = calls();
        return allLocations;
    }

    private synchronized ArrayList<Location> calls() {
        ArrayList<Location> temp = new ArrayList<>();
        addNewLocations(temp, getConsulats("1"));
        addNewLocations(temp, getMiradores("2"));
        addNewLocations(temp, getComisarias("3"));
        addNewLocations(temp, getMuelles("4"));
        addNewLocations(temp, getMuelles("4"));
        addNewLocations(temp, getPlayas("5"));
        addNewLocations(temp, getPuntosDeRecogidaDeArbolesDeNavidad("6"));
        addNewLocations(temp, getAreasDeRecreoParaPerros("7"));
        addNewLocations(temp, getEspaciosGimnasiaGenteMayor("8"));
        for (int i = 0; i < temp.size(); i++) {
            temp.get(i).set_id(i);
        }
        return temp;
    }

    private ArrayList<Location> addNewLocations(ArrayList<Location> temp, ArrayList<Location> locations) {
        boolean check = true;
        while (check){
            if (locations != null) check = false;
        }
        for (int i = 0; i < locations.size(); i++) {
            temp.add(locations.get(i));
        }
        return temp;
    }

    public ArrayList<Location> getConsulats(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","873e7692-cae0-4c7d-ac44-5cbc99843b39")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getMiradores(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","50e946a8-f4a2-4a16-8b40-6553bf4ccd81")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getComisarias(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","40ab2784-3555-466a-996c-55f3d900c0e5")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getMuelles(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","e17799cf-4029-4687-9ed2-e86bebc8fd9e")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getPlayas(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","cc4a6b67-4169-45ae-9f5a-e464908d2417")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getPuntosDeRecogidaDeArbolesDeNavidad(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","f7c8754a-0599-4d46-8838-3dc814886686")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getAreasDeRecreoParaPerros(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","fd4415e3-b7dc-4173-bb88-685401de191f")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getEspaciosGimnasiaGenteMayor(String typeLocation) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("resource_id","bee7897a-9088-4ab1-ba4b-de7f68cf7fc5")
                .build();
        String url = builtUri.toString();

        return doCall(url, typeLocation);
    }

    public ArrayList<Location> getPuntosComunidad(String typeLocation) {
        ArrayList<Location> locationsComunity = new ArrayList<>();
        DatabaseReference base = FirebaseDatabase
                .getInstance("https://easybcn-180222-default-rtdb.europe-west1.firebasedatabase.app/")
                .getReference();

        DatabaseReference locations = base.child("locations");
        Boolean check = Boolean.TRUE;
        locations.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Location location = snapshot.getValue(Location.class);
                locationsComunity.add(location);
                Log.d("LOCATION COMUNITY", location.toString());
                Log.d("SNAPSHOT", "snapshot: " + snapshot.hasChildren());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return locationsComunity;
    }


    private ArrayList<Location> doCall(String url, String typeLocation) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse, typeLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("SLEEP ERROR");
        }
        return null;
    }

    private ArrayList<Location> processJson(String jsonResponse, String typeLocation) {
        ArrayList<Location> cards = new ArrayList<>();
        try {

            JSONObject data = new JSONObject(jsonResponse);
            JSONObject jsonObject = data.getJSONObject("result");
            JSONArray jsonResults = jsonObject.getJSONArray("records");


            for (int i = 0; i < jsonResults.length(); i++) {
                JSONObject jsonCard = jsonResults.getJSONObject(i);

                Location location = new Location();
                location.set_id(jsonCard.getInt("_id"));
                location.setRegister_id(jsonCard.getString("register_id"));
                location.setNombre(jsonCard.getString("name"));
                location.setInstitution_name(jsonCard.getString("institution_name"));
                location.setAddresses_road_id(jsonCard.getInt("addresses_start_street_number"));
                location.setNombre_calle(jsonCard.getString("addresses_road_name"));
                location.setAddresses_neighborhood_id(jsonCard.getInt("addresses_neighborhood_id"));
                location.setNombre_barrio(jsonCard.getString("addresses_neighborhood_name"));
                location.setAddresses_district_id(jsonCard.getInt("addresses_district_id"));
                location.setNombre_distrito(jsonCard.getString("addresses_district_name"));
                location.setCodigo_postal(jsonCard.getInt("addresses_zip_code"));
                location.setAddresses_town(jsonCard.getString("addresses_town"));
                location.setValues_category(jsonCard.getString("values_category"));
                location.setValues_attribute_name(jsonCard.getString("values_attribute_name"));
                location.setValues_value(jsonCard.getString("values_value"));
                location.setCoordenada_x(jsonCard.getString("geo_epgs_4326_x"));
                location.setCoordenada_y(jsonCard.getString("geo_epgs_4326_y"));
                location.setTypeLocation(typeLocation);


                cards.add(location);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cards;
    }

}
