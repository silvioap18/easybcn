package com.example.proyectoeasybcn.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proyectoeasybcn.pojo.Location;

import java.util.List;

@Dao
public interface LocationDao {

    @Query("SELECT * FROM Location WHERE nombre like '%' || :name || '%' ")
    LiveData<List<Location>> getLocations(String name);

    @Query("SELECT * FROM Location WHERE nombre like '%' || :name || '%' & nombre = :type ORDER BY typeLocation")
    LiveData<List<Location>> getLocations(String name, String type);

    @Insert
    void addLocation(Location location);

    @Insert
    void addLocation(List<Location> locations);

    @Delete
    void deleteLocation(Location location);

    @Query("DELETE FROM Location")
    void deleteLocations();

}
